package pe.tasayco.users.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.tasayco.users.domain.*;

@RestController
@RequestMapping("/java/user")
public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private Environment env;
	
	@RequestMapping(method = RequestMethod.GET)
    public List<User> listUser(){
		LOGGER.info("GET users. Method:listUser");
		
		String value = env.getProperty("SERVER");
		LOGGER.info("VARIABLE SERVER: " + value);
		
		List<User> user = new ArrayList<>();
		
		User obj = new User();
		obj.setId(1);
		obj.setName("Jair");
		user.add(obj);
		
		User obj_2 = new User();
		obj_2.setId(2);
		obj_2.setName("Antonio");
		user.add(obj_2);
		
        return user;
    }
}
